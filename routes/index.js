const Router = require('express')

// const CategoryRouter = require('./categoryRouter')
// const ProductRouter = require('./productRouter')
// const SignInRouter = require('./signInRouter')
// const PostRouter = require('./postRouter')
// const UserRouter = require('./userRouter')
// const RawQueryRouter =require('./rawQueryRouter')

const StaffRouter = require('./staffRouter')
const RoomRouter = require("./roomRouter") 
const FruitTypeRouter = require('./fruitTypeRouter')
const FruitRouter = require('./fruitRouter')
const PartyRouter = require('./partyRouter')
const LoadRouter = require('./loadRouter')
const PlacementRouter = require('./placementRouter')

const router = new Router()

router.use('/staff', StaffRouter)
router.use('/fruitType', FruitTypeRouter)
router.use('/fruit', FruitRouter)
router.use('/party', PartyRouter)
router.use('/room', RoomRouter)
router.use('/load', LoadRouter)
router.use('/placement', PlacementRouter)

module.exports = router