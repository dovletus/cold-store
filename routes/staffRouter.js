const Router = require('express')
const { StaffController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', StaffController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', StaffController.getStaffById)
router.post('/criteria', StaffController.getStaffByCriteria)
router.post('/', StaffController.addStaff)
// // //auth routes
router.put('/', StaffController.updateStaff)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', StaffController.deleteStaff)

module.exports = router