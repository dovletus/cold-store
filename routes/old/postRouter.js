const Router = require('express')
const { PostController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', PostController.getPosts)
// router.get('/:id', PostController.getPostById)
router.get('/post', PostController.getPostById)
router.post('/', PostController.addPost)
// //auth routes
router.put('/', PostController.updatePost)
// router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', PostController.deletePost)

module.exports = router