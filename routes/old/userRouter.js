const Router = require('express')
const { UserController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', UserController.list)
// router.get('/:id', PostController.getPostById)
router.get('/user', UserController.getUserById)
router.post('/', UserController.addUser)
// // //auth routes
router.put('/', UserController.updateUser)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', UserController.deleteUser)

module.exports = router