const Router = require('express')
const { SignInController } = require('../controllers') // const SignInController =require('../controllers/signInController')

const router = new Router()

router.get('/', SignInController.refreshToken)
router.get('/signout', SignInController.signOut)
router.post('/', SignInController.signIn)
router.post('/register', SignInController.reqister)
router.post('/forgot-password', SignInController.forgotPassword)
// router.delete('/:id', SignInController.deleteCategory)

module.exports = router