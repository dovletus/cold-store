const Router = require('express')
const ProductController =require('../controllers/productController')

const router = new Router()

router.get('/', ProductController.getList)
router.get('/:id', ProductController.getProductById)
//auth routes
router.post('/', ProductController.addProduct)
router.put('/', ProductController.updateProduct)
router.delete('/:id', ProductController.deleteProduct)

module.exports = router