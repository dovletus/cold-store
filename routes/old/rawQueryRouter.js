const Router = require('express')
const  {RawQueryController } = require('../controllers')


const router = new Router()

router.get('/', RawQueryController.getUserList)


module.exports = router