const Router = require('express')
const CategoryController =require('../controllers/categoryController')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', CategoryController.getList)
router.get('/:id', CategoryController.getCategoryById)
//auth routes
router.post('/', authMiddleware, CategoryController.addCategory)
router.put('/', authMiddleware, CategoryController.updateCategory)
router.delete('/:id', authMiddleware, CategoryController.deleteCategory)

module.exports = router