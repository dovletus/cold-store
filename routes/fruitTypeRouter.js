const Router = require('express')
const { FruitTypeController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', FruitTypeController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', FruitTypeController.getFruitTypeById)
router.post('/', FruitTypeController.addFruitType)
// // //auth routes
router.put('/', FruitTypeController.updateFruitType)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', FruitTypeController.deleteFruitType)

module.exports = router