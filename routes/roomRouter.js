const Router = require('express')
const { RoomController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', RoomController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', RoomController.getRoomById)

router.post('/', RoomController.addRoom)
// // //auth routes
router.put('/', RoomController.updateRoom)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', RoomController.deleteRoom)

module.exports = router