const Router = require('express')
const { PartyController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', PartyController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', PartyController.getPartyById)

router.post('/', PartyController.addParty)
// // //auth routes
router.put('/', PartyController.updateParty)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', PartyController.deleteParty)

module.exports = router