const Router = require('express')
const { LoadController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', LoadController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', LoadController.getLoadById)

router.post('/', LoadController.addLoad)
// // //auth routes
router.put('/', LoadController.updateLoad)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', LoadController.deleteLoad)

module.exports = router