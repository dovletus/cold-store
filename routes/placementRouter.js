const Router = require('express')
const { PlacementController } = require('../controllers')
// const PostController =require('../controllers')
const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', PlacementController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', PlacementController.getById)

router.post('/', PlacementController.add)
// // //auth routes
router.put('/', PlacementController.update)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', PlacementController.delete)

module.exports = router