// const dotenv = require("dotenv") 
require("dotenv").config()

const express = require('express')
const path = require('path')
const cors = require('cors')
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer({ dest: "uploads/" });
// var upload = multer({ dest: "uploads/" });
const sequelize = require("./database/db");

const Controller = require('./controllers')
const router =require('./routes')

const readDB = require('./database/config')

const app = express()

// dotenv.config()
const port = process.env.PORT || 8001

//send static files
app.use('/static', express.static(path.resolve(__dirname,'public')))
app.use(cors())

//for parsing application/json
app.use(express.json())

// app.use(express.urlencoded({extended:true} ))

// for parsing application/xwww-
// app.use(bodyParser.urlencoded({ extended: true }));

//for parsing form-data
app.use(upload.array());

app.use('/api', router)

app.post("/upload", upload.array("files"), (req, res) => {
    console.log(req.body);
    console.log(req.files);
    res.json({ message: "Successfully uploaded files" });
})  ;

// app.get('/testDb', readDB )

const startServer = async ()=> {
let hasConnected = true

    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        await sequelize.sync()
      } catch (error) {
        hasConnected = false
        console.error('Unable to connect to the database:', error);
      }

    try {
        // await sequelize.authenticate()
        // await sequelize.sync() //re-configuration
        if (hasConnected){            
            app.listen(port,()=>{
                console.log('server started on port',port)
            })
        } else {
            console.log('Unable to start server..')
        }
    } catch (error) {
        console.log('Error during startup..',error)
    }
}

startServer()
