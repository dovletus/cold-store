const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const FruitType = sequelize.define("fruitType",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type:DataTypes.STRING,
        defaultValue:'',      
    },
    img: {
        type:DataTypes.STRING,
        defaultValue:'',      
    }
})

module.exports = {
    FruitType
}