const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const Room = sequelize.define("room",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type:DataTypes.STRING,
        defaultValue:'',      
    },
    intended: {
        type:DataTypes.STRING,
        defaultValue:'',
    },
    temperature: {
        type:DataTypes.INTEGER,
        defaultValue:'',
    },
    humidity: {
        type:DataTypes.INTEGER,
        defaultValue:'',
    },
    volume: {
        type:DataTypes.INTEGER,
        defaultValue:0,
    },
})

module.exports = {
    Room
}