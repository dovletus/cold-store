const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const typeID = {
    type: DataTypes.INTEGER,
    allowNull: false, 
}

const Placement = sequelize.define("placement",{
    id:{
        ...typeID,
        primaryKey:true,
        autoIncrement: true,
    },
    loadId: {...typeID},
    roomId : {...typeID},
    qty : {
        type: DataTypes.INTEGER,        
        allowNull:false,     
    },    
})

module.exports = {
    Placement
}