const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const typeID = {
    type: DataTypes.INTEGER,
    allowNull: false, 
}

const Load = sequelize.define("load",{
    id:{
        // type: DataTypes.INTEGER,
        // allowNull: false,
        ...typeID,
        primaryKey:true,
        autoIncrement: true,
        
    },
    partyId: {...typeID},
    fruitId : {...typeID},
    qty : {
        type: DataTypes.INTEGER,        
        allowNull:false,     
    },    
    price : {
        type:DataTypes.INTEGER,        
        allowNull:false,     
    },
    
})

module.exports = {
    Load
}