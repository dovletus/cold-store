const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const Party = sequelize.define("party",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },  
    staffId: {
        type:DataTypes.INTEGER,
        allowNull:false, 
    },
    totalQty: {
        type:DataTypes.INTEGER,
        allowNull:false, 
        defaultValue: 0,      
    },
    firm : {
        type: DataTypes.STRING,        
        allowNull:false,     
    },
    country : {
        type:DataTypes.STRING,        
        allowNull:false,     
    },
    truck : {
        type:DataTypes.STRING,        
        allowNull:false,     
    }
    
})

module.exports = {
    Party
}