const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const Staff = sequelize.define("staff",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type:DataTypes.STRING,
        defaultValue:'',
        validate:{
            max:50,            
        }        
    },
    login : {
        type:DataTypes.STRING,        
        unique:true,
        allowNull:false,
        validate:{
            max:50,            
        }        
    },
    password: {
        type:DataTypes.STRING,
        allowNull:false,
    },
    email: {
        type:DataTypes.STRING,
        defaultValue:'',  
    },
    tel: {
        type:DataTypes.STRING,
        defaultValue:'',  
    },
    web: {
        type:DataTypes.STRING,
        defaultValue:'',  
    },
    
})

module.exports = {
    Staff
}