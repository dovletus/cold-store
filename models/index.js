const { Fruit } = require("./fruit");
const { FruitType } = require("./fruitType");
const { Load } = require("./load");
const { Party } = require("./party");
const { Placement } = require("./placement");
const { Room } = require("./room");
const { Staff } = require("./staff");

//Relationships

//FruitType-Fruit
FruitType.hasMany(Fruit, {foreignKey: 'fruitTypeId'}) 
Fruit.belongsTo(FruitType)

//Party-Load
Party.hasMany(Load, { foreignKey: 'partyId'})
Load.belongsTo(Party)

//Staff-Party
Staff.hasMany(Party, {foreignKey: 'staffId'})
Party.belongsTo(Staff)

//Fruit-Load
Fruit.hasMany(Load, {foreignKey: 'fruitId'})
Load.belongsTo(Fruit)

//Placement-Load, Placement-Room
Load.hasMany(Placement, {foreignKey: 'loadId'})
Placement.belongsTo(Load)
Room.hasMany(Placement, {foreignKey: 'roomId'})
Placement.belongsTo(Room)

module.exports = {
  Staff,
  Fruit,
  FruitType,
  Party,
  Room,
  Load,
  Placement
};
