const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const Fruit = sequelize.define("fruit",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type:DataTypes.STRING,
        defaultValue:'',      
    },
    img: {
        type:DataTypes.STRING,
        defaultValue:'',      
    },
    fruitTypeId: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
})

module.exports = {
    Fruit
}