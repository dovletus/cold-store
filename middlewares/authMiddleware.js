// const jwt = require('jsonwebtoken')

function authMiddleware (req, res, next){
    if (req.method === 'OPTIONS'){
        next()
    }
    // {Authorization: 'Bearer payload'}
    const token = req.headers.authorization//.split(' ')[1]        
    
    if (!token){
        return  res.status(401).json({message: 'no token'})
    }
    
    try {
        const data = JSON.parse(token)

        req.user = data.user
        next()
    } catch (err) {
        res.status(401).json({message: 'invalid token',err})
    }
}

module.exports = authMiddleware