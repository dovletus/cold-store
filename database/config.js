const mysql = require('mysql')

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
})



const readDB = async (req,res)=>
{   
    connection.connect()
    connection.query('SELECT * from posts', (err, rows, fields) => {
        if (err) throw err

        connection.end()
        console.log('rows:',rows)
        res.json(rows);
    })
}


module.exports = readDB