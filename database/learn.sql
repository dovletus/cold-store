-- Select all fields from posts table
Select * from posts;

-- Select some fields from posts table
Select id, title, created_at from posts;

-- Select fields based on a criteria
Select * From posts Where title = 'post 1';
Select * From posts Where posts.id < 2;
Select * From posts Where seen >= 5 ;
Select * From posts Where title like '%2%';
Select * From posts Where seen between 10 and 100;

Select * From posts Where seen >= 5 AND title like '%2%';

-- Order by 
-- Asc (Ascending - kopelyan), Desc (descending - azalyan)
Select * from posts Order by seen Asc;
Select * from posts Order by seen DESC;

-- Add new record (row)
Insert into posts (title, description, author, seen) Values ('new post','description for the new post', 'SQL', 23);
-- Add value for all fields (columns)
Insert into posts Values ('new post 2','description for the new post 2', 'SQL', 33);

-- Update
Update posts Set title = 'updated new post twice' Where id = 4
Update posts Set title = 'updated new post' Where title like '%new%'

-- Update many fields
Update posts Set title = 'upd post 4', description='upd description', author='Developer' Where id = 4

-- Delete records (rows)
Delete from posts Where id = 5; 