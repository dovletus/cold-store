const {v4 : uuidv4} = require('uuid')
var md5 = require('md5');


const {Op} = require('sequelize')
const { Staff } = require('../models')

class StaffController {

    async list(req, res, next){
        const staffs = await Staff.findAndCountAll({raw: true, })
        
        //remove passwords
        const sanitisedOutput = {
            // count: staffs.count, rows:staffs.rows.map(item=>({...item.dataValues , password:""}))
            count: staffs.count, rows:staffs.rows.map(staff=>{
                delete staff.password;
                return staff
            })

        }
        res.json(sanitisedOutput)

    }

    async getStaffById(req, res, next){
        const {id} = req.params;   

        const staff = await Staff.findOne({raw:true, 
            where:{id}
             } )             
        res.json(staff)
    }
    
    async getStaffByCriteria(req, res, next){
        const {id, name, login} = req.body;   
        console.log('id name login',id, name, login)

        const criteria = []
        id && criteria.push({id:id})
        name && criteria.push({name:name})
        login && criteria.push({login:login})
        
        // if(id) criteria.push({id:id})
        // if(name) criteria.push({name:name})
        // if(login) criteria.push({login:login})

        // const category = await User.findOne({where:{id}, 
        //     include: Post } )

        const staff = await Staff.findOne({raw:true, 
            where:
            {
                [Op.and]:[...criteria]
            } 
             } ) 
        
             delete staff.password            
        res.json(staff)
    }

    async addStaff(req, res, next){
        const {name, login, password, email, tel, web} = req.body
        const staff = await Staff.create( {name, login, password:md5(password), email, tel, web});
        // console.log("Staff", staff)
        res.json({success: true, newStaff: staff})
    }

    async updateStaff(req,res, next){
        const staff = req.body
        
        //check for password
        if (staff.password) staff.password = md5(staff.password)
        
        const result = await Staff.update(staff, {where:{id:staff.id}})
        res.json({result:result[0]})
    }

    async deleteStaff(req, res, next){
        const id = req.params.id        
        
        const result = await Staff.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new StaffController()