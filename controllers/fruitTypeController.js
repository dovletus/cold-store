const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { FruitType } = require('../models')

class FruitTypeController {

    async list(req, res, next){
        const staffs = await FruitType.findAndCountAll({raw:true,})
        // console.log('posts',posts)
        res.json(staffs)

    }

    async getFruitTypeById(req, res, next){
        const {id, name, login} = req.query;

        const criteria = []
        id && criteria.push({id:id})
        name && criteria.push({name:name})
        login && criteria.push({login:login})
        
        // if(id) criteria.push({id:id})
        // if(name) criteria.push({name:name})
        // if(login) criteria.push({login:login})

        // const category = await User.findOne({where:{id}, 
        //     include: Post } )

        const category = await FruitType.findOne({raw:true,
            where:
            {
                [Op.and]:[...criteria]
            }, 
        } )

        res.json(category)
    }

    async addFruitType(req, res, next){
        const {id, name, login, password, email, tel, web} = req.body
        const fruitType = await FruitType.create( {id, name, login, password, email, tel, web});
        // console.log("FruitType", fruitType)
        res.json({success: true, newStaff: fruitType})
    }

    async updateFruitType(req,res, next){
        const fruitType = req.body
        const result = await FruitType.update(fruitType, {where:{id:fruitType.id}})
        res.json({result:result[0]})
    }

    async deleteFruitType(req, res, next){
        const id = req.params.id        
        
        const result = await FruitType.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new FruitTypeController()