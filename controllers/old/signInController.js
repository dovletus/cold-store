require('dotenv').config()
const {v4 : uuidv4} = require('uuid')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const generateJwt = (id, login, role) => {
    return jwt.sign({ id, login, role }, process.env.SECRET_KEY, { expiresIn: '24h' })
}

const verifyToken = (token)=> jwt.verify(token, process.env.SECRET_KEY)

const psw = '1234'

let hasUserSignedIn =false


class SignInController {
    //POST
    async signIn(req, res, next){
        const {login, password} = req.body

        if (login && password){
            const hashPassword = await bcrypt.hash(psw, 5)

            let comparePassword = bcrypt.compareSync(password, hashPassword)
            if (comparePassword) {
                const token = generateJwt(uuidv4(), login, 'user')
                hasUserSignedIn = true
                res.json({success: true, token}) 
            } else {
                res.json({success: false, message:'invalide password'})                
            }

        } else {
            res.json({success: false, message: 'provide valid credentials'})
        }        
    }

    //GET
    signOut(req, res, next){
        let token=''
        try {
            token = req.headers.authorization.split(' ')[1]
        } catch (err) {
            return res.json({success: false, message: 'Token error..',err})
        }

        try {
            const decode = verifyToken(token)
            // console.log('result', decode)
            
            if(decode){
                hasUserSignedIn = false
                return res.json({success: true})
            } 
            
            res.json({success: false, message: 'Token not verified'})
        } catch (err){
            res.json({success: false, message: 'Invalid token'})
        }

    }

    //POST
    reqister(req,res, next){
        const user = req.body //return {} if no data present

        const userKeys  = Object.keys(user)
                
        if(userKeys.length) {
            const newUser = {...user, id: uuidv4()}
            return res.json({success: true, newUser})
        }

        res.json({success: false, message:'No user data provided'})
    }

    forgotPassword(req, res, next){
        // const id = req.params.id        
        // const newList = db.categories.filter(cat=>cat.id != id)
        // const deletedCategory = newList.find(cat=>cat.id == id)
        // res.send(deletedCategory ? false : true)
        // // res.json(deletedCategory )
        res.send('not implemented yet')
    }
    
    //GET '/'
    refreshToken(req, res, next){
        let token=''
        try {
            token = req.headers.authorization.split(' ')[1]
        } catch (err) {
            return res.json({success: false, message: 'Token error..',err})
        }

        try {
            const decode = verifyToken(token)
            // console.log('result', decode)
            
            if(decode){
                const {id, login, role} = decode
               const newToken = generateJwt(id, login, role)
                return res.json({success: true, newToken})
            } 
            
            res.json({success: false, message: 'Token not verified'})
        } catch (err){
            res.json({success: false, message: 'Invalid token'})
        }

    }
}

module.exports = new SignInController()