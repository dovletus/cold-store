const sequelize = require('../database/db')
const { QueryTypes } = require('sequelize');

class RawQueryController {
    async getUserList(req, res, next){
        const result = await sequelize.query("SELECT count(*) AS `count` FROM `users`", { type: QueryTypes.SELECT })
        
        const users = await sequelize.query("SELECT * FROM `users` LIMIT 2", { type: QueryTypes.SELECT });

        res.json({count:result[0].count, users})
    }
}

module.exports = new RawQueryController()
