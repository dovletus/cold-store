const StaffController=require('./staffController')
const FruitTypeController = require('./fruitTypeController')
const FruitController = require('./fruitController')
const PartyController = require('./partyController')
const RoomController = require('./roomController')
const LoadController = require('./loadController')
const PlacementController = require('./placementController')


module.exports = { 
                    StaffController,
                    FruitTypeController,
                    FruitController,
                    PartyController,
                    RoomController,
                    LoadController,
                    PlacementController
                    }