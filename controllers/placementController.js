const {Op} = require('sequelize')
const { Placement, Room, Load} = require('../models')

class PlacementController {

    async list(req, res, next){
        const placements = await Placement.findAndCountAll({raw: true, })
        res.json(placements)

    }

    async getById(req, res, next){
        const {id} = req.params;
       
        const placement = await Placement.findOne(
            {
                // raw:true,
                where:{id},
                include: [Load, Room]
             } )

        res.json(placement)
    }

    async add(req, res, next){
        const {roomId,  loadId, qty} = req.body
        const placement = await Placement.create( {roomId,  loadId, qty});
        // console.log("Fruit", staff)
        res.json({success: true, newPlacement: placement})
    }

    async update(req,res, next){
        const placement = req.body
        const result = await Placement.update(placement, {where:{id:placement.id}})
        res.json({result:result[0]})
    }

    async delete(req, res, next){
        const id = req.params.id        
        
        const result = await Placement.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new PlacementController()