const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { Load, Party, Fruit } = require('../models')

class LoadController {

    async list(req, res, next){
        const loads = await Load.findAndCountAll({raw: true, })
        // console.log('posts',posts)0
        res.json(loads)

    }

    async getLoadById(req, res, next){
        const {id} = req.params;
       
        const load = await Load.findOne({
            // raw:true,
            where:{id},
            include: [Party, Fruit]     
        } )

        res.json(load)
    }

    // async getFruitByCriteria(req, res, next){
    //     const {id, name} = req.body;

    //     const criteria = []
    //     id && criteria.push({id:id})
    //     name && criteria.push({name:name})

    //     const category = await Fruit.findOne({raw:true,where:
    //         {
    //             [Op.and]:[...criteria]
    //         }, 
    //         } )

    //     res.json(category)
    // }

    async addLoad(req, res, next){
        const {partyId, fruitId, price, qty} = req.body
        const load = await Load.create( {partyId, fruitId, price, qty});
        // console.log("Fruit", staff)
        res.json({success: true, newLoad: load})
    }

    async updateLoad(req,res, next){
        const load = req.body
        const result = await Load.update(load, {where:{id:load.id}})
        res.json({result:result[0]})
    }

    async deleteLoad(req, res, next){
        const id = req.params.id        
        
        const result = await Load.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new LoadController()