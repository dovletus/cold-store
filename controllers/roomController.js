const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { Room } = require('../models')

class RoomController {

    async list(req, res, next){
        const rooms = await Room.findAndCountAll({raw: true, })
        // console.log('posts',posts)0
        res.json(rooms)

    }

    async getRoomById(req, res, next){
        const {id} = req.params;
       
        const room = await Room.findOne({raw:true,where:{id}
             } )

        res.json(room)
    }

    async getFruitByCriteria(req, res, next){
        const {id, name} = req.body;

        const criteria = []
        id && criteria.push({id:id})
        name && criteria.push({name:name})

        const category = await Fruit.findOne({raw:true,where:
            {
                [Op.and]:[...criteria]
            }, 
            } )

        res.json(category)
    }

    async addRoom(req, res, next){
        const {id,  name, intended, temperature,humidity, volume } = req.body
        const room = await Room.create( {id, name, intended, temperature, humidity, volume});
        // console.log("Fruit", staff)
        res.json({success: true, newRoom: room})
    }

    async updateRoom(req,res, next){
        const room = req.body
        const result = await Room.update(room, {where:{id:room.id}})
        res.json({result:result[0]})
    }

    async deleteRoom(req, res, next){
        const id = req.params.id        
        
        const result = await Room.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new RoomController()