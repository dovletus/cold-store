const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { Party, Staff } = require('../models')

class PartyController {

    async list(req, res, next){
        const parties = await Party.findAndCountAll({raw: true, })
        // console.log('posts',posts)0
        res.json(parties)

    }

    async getPartyById(req, res, next){
        const {id} = req.params;
       
        const party = await Party.findOne(
            {
                // raw:true,
                where:{id},
                include: Staff
             } )

        res.json(party)
    }

    // async getFruitByCriteria(req, res, next){
    //     const {id, name} = req.body;

    //     const criteria = []
    //     id && criteria.push({id:id})
    //     name && criteria.push({name:name})

    //     const category = await Fruit.findOne({raw:true,where:
    //         {
    //             [Op.and]:[...criteria]
    //         }, 
    //         } )

    //     res.json(category)
    // }

    async addParty(req, res, next){
        const {staffId,  totalQty, firm, country, truck} = req.body
        const party = await Party.create( {staffId, totalQty, firm, country, truck});
        // console.log("Fruit", staff)
        res.json({success: true, newParty: party})
    }

    async updateParty(req,res, next){
        const party = req.body
        const result = await Party.update(party, {where:{id:party.id}})
        res.json({result:result[0]})
    }

    async deleteParty(req, res, next){
        const id = req.params.id        
        
        const result = await Party.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new PartyController()